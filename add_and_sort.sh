#!/bin/bash

# Add a line to the exceptions.txt file, then re-sort

DATA_FILE=exceptions.txt
TEMP_FILE=temp.txt

for term
do
    echo $term 
done >> $DATA_FILE

mv $DATA_FILE $TEMP_FILE
sort $TEMP_FILE > $DATA_FILE
rm $TEMP_FILE
